# Whitefox

Name = Ducky YotH;
Version = 0.8;
Author = "TerminalMan";
KLL = 0.5;

# Modified Date
Date = 2019-01-11;


#
# Defines available to the Whitefox Scan Module
#


# ScanCode Strobe List
# This specified which the column (strobe) GPIOs for the matrix
# gpio(<label>, <index>)
# gpio(A,3)  ==> PA3
# gpio(C,24) ==> PC24
ScanCodeStrobeList => ScanCodeStrobeList_define;
ScanCodeStrobeList = "
	gpio(B,2),
	gpio(B,3),
	gpio(B,18),
	gpio(B,19),
	gpio(C,0),
	gpio(C,8),
	gpio(C,9),
	gpio(C,10),
	gpio(C,11)
";


# ScanCode Sense List
# This specified which the row (sense) GPIOs for the matrix
# gpio(<label>, <index>)
# gpio(A,3)  ==> PA3
# gpio(C,24) ==> PC24
ScanCodeSenseList => ScanCodeSenseList_define;
ScanCodeSenseList = "
	gpio(D,0),
	gpio(D,1),
	gpio(D,4),
	gpio(D,5),
	gpio(D,6),
	gpio(D,7),
	gpio(C,1),
	gpio(C,2)
";


# Debug LED
ledDebugPin = "gpio(A,5)";


# Driver Chip
ISSI_Chip_31FL3733 = 1;

# Available ISSI Chips
ISSI_Chips = 1;

# I2C Buses
ISSI_I2C_Buses = 1; # 1 by default

# I2C LED Struct Definition
LED_BufferStruct = "
typedef struct LED_Buffer {
	uint16_t i2c_addr;
	uint16_t reg_addr;
	uint16_t ledctrl[0];
	uint16_t unused[0];
	uint16_t buffer[192];
} LED_Buffer;
volatile LED_Buffer LED_pageBuffer[ ISSI_Chips_define ];

typedef struct KLL_Buffer {
	uint16_t buffer[192];
} KLL_Buffer;
volatile KLL_Buffer KLL_pageBuffer[ ISSI_Chips_define ];
";


ISSI_Global_Brightness = 90; # Half brightness, approx. 1 A @ All White

# FPS Target
# Each ISSI chip setup has a different optimal framerate.
# This setting specifies a target frame rate. This is sort've like "V-Sync" on monitors.
# So the framerate will not go above this amount.
# If the framerate goes below, ledFPS cli will issue warnings when enabled.
ISSI_FrameRate_ms => ISSI_FrameRate_ms_define;
ISSI_FrameRate_ms = 10; # 1000 / <ISSI_FrameRate_ms> = 100 fps


# LED Default Enable Mask Override
#
# Each LED is represented by a single bit
# See (http://www.issi.com/WW/pdf/31FL3731C.pdf) for details
ISSILedMask1 = "
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, /* CS1 -> CS16 (SW1  -> SW4)  */
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xBF, 0xFF, 0xBF, /* CS1 -> CS16 (SW5  -> SW8)  */
	0xFF, 0xBF, 0xFF, 0xBF, 0xFF, 0xBF, 0xFF, 0xBF, /* CS1 -> CS16 (SW9  -> SW12) */
";

### Pixel Buffer Setup ###
# Defines channel mappings, changing the order will affect Pixel definitions
Pixel_Buffer_Size[]    =   0; # Starting channel for each buffer
Pixel_Buffer_Width[]   =  16; # Width of each channel buffer (may be different than effective channel size)
Pixel_Buffer_Length[]  = 192; # Length of each buffer (count, not bytes)
Pixel_Buffer_Buffer[0] = "KLL_pageBuffer[0].buffer"; # Pointer to the start of the buffer

LED_Buffer_Size[]    =   0; # Starting channel for each buffer
LED_Buffer_Width[]   =  16; # Width of each channel buffer (may be different than effective channel size)
LED_Buffer_Length[]  = 192; # Length of each buffer (count, not bytes)
LED_Buffer_Buffer[0] = "LED_pageBuffer[0].buffer"; # Pointer to the start of the buffer

# Channel Optimizations
Pixel_HardCode_ChanWidth = 8;
Pixel_HardCode_Channels = 3;



### Pixel Display Mapping Parameters ###
Pixel_DisplayMapping_UnitSize = 19; # Default unit spacing in mm
Pixel_DisplayMapping_ColumnSize = 1;
Pixel_DisplayMapping_RowSize = 2;
Pixel_DisplayMapping_ColumnDirection = -1;
Pixel_DisplayMapping_RowDirection = 1;



### Pixel Mapping ###
# Organizes each of the channels into pixels (may, or may not be multi-channel)

# 48 49 50 51 52 53 54 55 56 57 58 59 60 61
# 33 34 35 36 37 38 39 40 41 42 43 44 45 62
# 17 18 19 20 21 22 23 24 25 26 27 28 46
#  1  2  3  4  5  6  7  8  9 10 11 12 29
# 16 15 14          13       32 31 30 47


P[  1]( 16:8,   0:8,  32:8) : S1;
P[  2]( 17:8,   1:8,  33:8) : S2;
P[  3]( 18:8,   2:8,  34:8) : S3;
P[  4]( 19:8,   3:8,  35:8) : S4;
P[  5]( 20:8,   4:8,  36:8) : S5;
P[  6]( 21:8,   5:8,  37:8) : S6;
P[  7]( 22:8,   6:8,  38:8) : S7;
P[  8]( 23:8,   7:8,  39:8) : S8;
P[  9]( 24:8,   8:8,  40:8) : S9;
P[ 10]( 25:8,   9:8,  41:8) : S10;
P[ 11]( 26:8,  10:8,  42:8) : S11;
P[ 12]( 27:8,  11:8,  43:8) : S12;
P[ 13]( 28:8,  12:8,  44:8) : S13;
P[ 14]( 29:8,  13:8,  45:8) : S14;
P[ 15]( 30:8,  14:8,  46:8) : S15;
P[ 16]( 31:8,  15:8,  47:8) : S16;
P[ 17]( 64:8,  48:8,  80:8) : S17;
P[ 18]( 65:8,  49:8,  81:8) : S18;
P[ 19]( 66:8,  50:8,  82:8) : S19;
P[ 20]( 67:8,  51:8,  83:8) : S20;
P[ 21]( 68:8,  52:8,  84:8) : S21;
P[ 22]( 69:8,  53:8,  85:8) : S22;
P[ 23]( 70:8,  54:8,  86:8) : S23;
P[ 24]( 71:8,  55:8,  87:8) : S24;
P[ 25]( 72:8,  56:8,  88:8) : S25;
P[ 26]( 73:8,  57:8,  89:8) : S26;
P[ 27]( 74:8,  58:8,  90:8) : S27;
P[ 28]( 75:8,  59:8,  91:8) : S28;
P[ 29]( 76:8,  60:8,  92:8) : S29;
P[ 30]( 77:8,  61:8,  93:8) : S30;
P[ 31]( 78:8,  62:8,  94:8) : S31;
P[ 32]( 79:8,  63:8,  95:8) : S32;
P[ 33](112:8,  96:8, 128:8) : S33;
P[ 34](113:8,  97:8, 129:8) : S34;
P[ 35](114:8,  98:8, 130:8) : S35;
P[ 36](115:8,  99:8, 131:8) : S36;
P[ 37](116:8, 100:8, 132:8) : S37;
P[ 38](117:8, 101:8, 133:8) : S38;
P[ 39](118:8, 102:8, 134:8) : S39;
P[ 40](119:8, 103:8, 135:8) : S40;
P[ 41](120:8, 104:8, 136:8) : S41;
P[ 42](121:8, 105:8, 137:8) : S42;
P[ 43](122:8, 106:8, 138:8) : S43;
P[ 44](123:8, 107:8, 139:8) : S44;
P[ 45](124:8, 108:8, 140:8) : S45;
P[ 46](125:8, 109:8, 141:8) : S46;
P[ 47](127:8, 111:8, 143:8) : S47;
P[ 48](160:8, 144:8, 176:8) : S48; #ESC B0 A0 90 SW12 SW11 SW10 CS1
P[ 49](161:8, 145:8, 177:8) : S49;
P[ 50](162:8, 146:8, 178:8) : S50;
P[ 51](163:8, 147:8, 179:8) : S51;
P[ 52](164:8, 148:8, 180:8) : S52;
P[ 53](165:8, 149:8, 181:8) : S53;
P[ 54](166:8, 150:8, 182:8) : S54;
P[ 55](167:8, 151:8, 183:8) : S55;
P[ 56](168:8, 152:8, 184:8) : S56;
P[ 57](169:8, 153:8, 185:8) : S57;
P[ 58](170:8, 154:8, 186:8) : S58;
P[ 59](171:8, 155:8, 187:8) : S59;
P[ 60](172:8, 156:8, 188:8) : S60;
P[ 61](173:8, 157:8, 189:8) : S61;
P[ 62](175:8, 159:8, 191:8) : S62;


### LED Default Fade Groups ###
#
# Group 0 -> Keys
# Group 1 -> Underlighting
# Group 2 -> Indicators
# Group 3 -> Active layer (not default)
KLL_LED_FadeGroup[0] = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62";
KLL_LED_FadeGroup[1] = "";
KLL_LED_FadeGroup[2] = "";

S0x01 : U"Esc";
S0x02 : U"1";
S0x03 : U"2";
S0x04 : U"3";
S0x05 : U"4";
S0x06 : U"5";
S0x07 : U"6";
S0x08 : U"7";
S0x09 : U"8";
S0x0a : U"9";
S0x0B : U"0";
S0x0C : U"Minus";
S0x0D : U"Equal";
S0x0E : U"Hash";
S0x0F : U"Backspace";
S0x10 : U"BackTick";
S0x11 : U"Tab";
S0x12 : U"Q";
S0x13 : U"W";
S0x14 : U"E";
S0x15 : U"R";
S0x16 : U"T";
S0x17 : U"Y";
S0x18 : U"U";
S0x19 : U"I";
S0x1a : U"O";
S0x1B : U"P";
S0x1C : U"LBrace";
S0x1D : U"RBrace";
S0x1E : U"Backslash";
S0x1F : U"Delete";
S0x20 : U"Function1";
S0x21 : U"A";
S0x22 : U"S";
S0x23 : U"D";
S0x24 : U"F";
S0x25 : U"G";
S0x26 : U"H";
S0x27 : U"J";
S0x28 : U"K";
S0x29 : U"L";
S0x2a : U"Semicolon";
S0x2B : U"Quote";
S0x2C : U"Backslash";
S0x2D : U"Enter";
S0x2E : U"PageUp";
S0x2F : U"Shift";
S0x30 : U"`";
S0x31 : U"Z";
S0x32 : U"X";
S0x33 : U"C";
S0x34 : U"V";
S0x35 : U"B";
S0x36 : U"N";
S0x37 : U"M";
S0x38 : U"Comma";
S0x39 : U"Period";
S0x3a : U"Slash";
S0x3B : U"Function1";
S0x3C : U"Up";
S0x3D : U"PageDown";
S0x3E : U"Ctrl";
S0x3F : U"LGUI";
S0x40 : U"LAlt";
S0x41 : U"Space";
S0x42 : U"Left";
S0x43 : U"Down";

S0x45 : U"Up";
S0x46 : U"Right";



### Physical Positions ###

# 01  02  03  04  05  06  07  08  09  10  11  12  13  14   15

#  17  18  19  20  21  22  23  24  25  26  27  28  30
#                                                         45
#   32  33  34  35  36  37  38  39  40  41  42  43  44

#  47  48  49  50  51  52  53  54  55  56  57  58       59

#  62    63    65            65            66    67    69    70


## Rows
S[1-16]  <= y:0;
S[17-31] <= y:-19.05;
S[32-46] <= y:-38.10;
S[47-61] <= y:-57.15;
S[62] <= y:-76.20;

## Columns
# Top Row
S[1] <= x:0;
S[2] <= x:19.05;
S[3] <= x:38.1;
S[4] <= x:57.15;
S[5] <= x:76.2;
S[6] <= x:95.25;
S[7] <= x:114.3;
S[8] <= x:133.35;
S[9] <= x:152.4;
S[10] <= x:171.45;
S[11] <= x:190.5;
S[12] <= x:209.55;
S[13] <= x:228.6;
S[14] <= x:247.65; # x
S[15] <= x:266.7; # x

S[16,31,46,61] <= x:285.75;

# Top Alphabet Row
S[17] <= x:4.76;
S[18] <= x:27.81;
S[19] <= x:46.86;
S[20] <= x:65.91;
S[21] <= x:84.96;
S[22] <= x:104.01;
S[23] <= x:123.06;
S[24] <= x:142.11;
S[25] <= x:161.16;
S[26] <= x:180.21;
S[27] <= x:199.26;
S[28] <= x:218.31;
S[29] <= x:237.36;
S[30] <= x:260.41; # x

# Middle Alphabet Row
S[32] <= x:7.14;
S[33] <= x:33.14;
S[34] <= x:52.19;
S[35] <= x:71.24;
S[36] <= x:90.29;
S[37] <= x:109.34;
S[38] <= x:128.39;
S[39] <= x:147.44;
S[40] <= x:166.49;
S[41] <= x:185.54;
S[42] <= x:204.59;
S[43] <= x:223.64;
##S[44] <= x:242.59; # x
S[45] <= x:254.64; # x

# Bottom Alphabet Row
S[47] <= x:11.9; # x
#S[48] <= x:; # x
S[49] <= x:42.9;
S[50] <= x:61.95;
S[51] <= x:81;
S[52] <= x:100.05;
S[53] <= x:119.1;
S[54] <= x:138.15;
S[55] <= x:157.2;
S[56] <= x:176.25;
S[57] <= x:195.3;
S[58] <= x:214.35;
S[59] <= x:240.35; # x

S[60] <= x:266.7;

# Bottom Row
S[62] <= x:2.38; # x

